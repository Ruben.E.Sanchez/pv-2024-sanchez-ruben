package ar.edu.unju.fi.pv.tp6.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class GestionAlumno {
	//Creacion de la coleccion de alumnos
	public static void coleccionAlumno(List<Alumno> listaAlumnos) {
			
		Alumno alumno1 = new Alumno("APU3647",42584968, "Ruben Sanchez", "Apu", LocalDate.of(1996, 10, 4));
		alumno1.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno1);
		
		Alumno alumno2 = new Alumno("APU2658",42584968, "joaquin Quintana", "Apu", LocalDate.of(1998, 4, 22));
		alumno2.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno2);
		
		Alumno alumno3 = new Alumno("APU5248",45258658, "Pedro Paez", "Apu", LocalDate.of(2001, 8, 12));
		alumno3.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno3);
		
		Alumno alumno4 = new Alumno("APU5248",42151258, "Belen Sosa", "Lic. Sistema", LocalDate.of(2002, 6, 28));
		alumno4.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno4);
		
		Alumno alumno5 = new Alumno("APU5248",36548124, "Martin Roca", "Ingenieria Informatica", LocalDate.of(1998, 7, 5));
		alumno5.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno5);
		
		Alumno alumno6 = new Alumno("APU5248",34225417, "Lucia Portal", "Lic. Sistema", LocalDate.of(1999, 1, 19));
		alumno6.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno6);
		
		Alumno alumno7 = new Alumno("APU5248",41257359, "Marta Llanos", "Lic. Sistema", LocalDate.of(2001, 3, 3));
		alumno7.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno7);
		
		Alumno alumno8 = new Alumno("APU5248",38512469, "Julia Mendez", "Apu", LocalDate.of(1996, 10, 20));
		alumno8.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno8);
		
		Alumno alumno9 = new Alumno("APU5248",39658412, "Florencia Rivera", "Apu", LocalDate.of(1996, 4, 12));
		alumno9.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno9);
		
		Alumno alumno10 = new Alumno("APU5248",37854695, "Mariana Acosta", "Lic. Sistema", LocalDate.of(1997, 8, 8));
		alumno10.setFechaIngreso(LocalDate.now());
		listaAlumnos.add(alumno10);
	}
	//Metodo para agregar un nuevo alumno a la lista
	public static void agregarAlumno(Scanner entrada, List<Alumno> listaAlumnos) {
		// 
		System.out.println("NUEVO ALUMNO");
        System.out.print("\nLibreta Universitaria: ");
        String libretaUniversitaria = entrada.nextLine();
        entrada.nextLine();
        System.out.print("DNI: ");
        int dni = entrada.nextInt();
        entrada.nextLine();
        System.out.print("Nombre y Apellido: ");
        String apellido = entrada.nextLine();
        System.out.print("Carrera: ");
        String carrera = entrada.nextLine();

        System.out.print("Fecha de Nacimiento (dd/mm/yyyy): ");
        String fechaNacimientoStr= entrada.nextLine();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaNacimiento = LocalDate.parse(fechaNacimientoStr, formatter);
    		        
        listaAlumnos.add(new Alumno(libretaUniversitaria, dni, apellido, carrera, fechaNacimiento));
        System.out.println("Alumno Agregado Correctamente!...");
        
        System.out.println("\nPresione Enter para volver al Menu...");
        entrada.nextLine();
        entrada.nextLine();
	}
	
	//Metodo para mostrar a todos los alumnos de la lista y sus atributos
	public static void mostrarAlumno(Scanner entrada, List<Alumno> listaAlumnos) {
		// TODO Esbozo de método generado automáticamente
		System.out.println("\nLista de Alumnos: ");
		int i=1;
		for(Alumno alumno : listaAlumnos) {
			System.out.println("Alumno Nº "+i);
			System.out.println(alumno);
			i++;
		}
		System.out.println("\nPresione Enter para volver al Menu...");
        entrada.nextLine();
        entrada.nextLine();
	}

	//Metodo para buscar un alumno ingresando por consola su numero de libreta 
	public static void buscarAlumnoPorLibreta(Scanner entrada, List<Alumno> listaAlumnos) {
		// TODO Esbozo de método generado automáticamente
		System.out.println("Ingrese libreta universitaria a buscar:");
        String libreta = entrada.next();
        Alumno alumnoEncontrado = buscarAlumno(libreta, listaAlumnos);
        if (alumnoEncontrado != null) {
            System.out.println("\nAlumno encontrado:\n" + alumnoEncontrado);
        } else {
            System.out.println("Alumno no encontrado.");
        }
        System.out.println("\nPresione Enter para volver al Menu...");
        entrada.nextLine(); 
        entrada.nextLine();
	}
	//Metodo de busqueda del alumno
	public static Alumno buscarAlumno(String libreta, List<Alumno> listaAlumnos) {
        for (Alumno alumno : listaAlumnos) {
            if (alumno.getLibretaUniversitaria().equals(libreta)) {
                return alumno;
            }
        }
        return null;
    }
	//Metodo para contar la cantidad de alumnos ingresando un mes y año por consola
	public static int contarAlumnos(int mes, int anio, List<Alumno> listaAlumnos) {
        int contador = 0;
        for (Alumno alumno : listaAlumnos) {
            if (alumno.getFechaIngreso().getMonthValue() == mes && alumno.getFechaIngreso().getYear() == anio) {
                contador++;
            }
        }
        return contador; 
    }
}