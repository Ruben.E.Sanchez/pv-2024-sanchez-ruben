package ar.edu.unju.fi.pv.tp6.model;

import java.time.LocalDate;

//Definicion del Objeto
public class Alumno {
	private String libretaUniversitaria;
	private int dni;
	private String nombre;
	private String carrera;
	private LocalDate fechaNacimiento;
	private LocalDate fechaIngreso;

	// Constructor por Defecto 
	public Alumno() {
		super();
		// TODO Esbozo de constructor generado automáticamente
	}
	
	//Constructor definido con libreta universitaria, dni, nombre
	public Alumno(String libretaUniversitaria, int dni, String nombre) {
		super();
		this.libretaUniversitaria = libretaUniversitaria;
		this.dni = dni;
		this.nombre = nombre;
	}
	
	//Constructor definido menos fecha de ingreso
	public Alumno(String libretaUniversitaria, int dni, String nombre, String carrera, LocalDate fechaNacimiento) {
		super();
		this.libretaUniversitaria = libretaUniversitaria;
		this.dni = dni;
		this.nombre = nombre;
		this.carrera = carrera;
		this.fechaNacimiento = fechaNacimiento;
		this.setFechaIngreso(LocalDate.now());
	}
	//Metodos Setter y Getter
	public String getLibretaUniversitaria() {
		return libretaUniversitaria;
	}
	
	public void setLibretaUniversitaria(String libretaUniversitaria) {
		this.libretaUniversitaria = libretaUniversitaria;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	//Método to String
	@Override
	public String toString() {
		return "[libretaUniversitaria: " + libretaUniversitaria + ", Dni: " + dni + ", Nombre: " + nombre
				+ ", Carrera: " + carrera + ", FechaNacimiento: " + fechaNacimiento + ", FechaIngreso=" + fechaIngreso
				+ "]";
	}
}
