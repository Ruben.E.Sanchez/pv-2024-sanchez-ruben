package ar.edu.unju.fi.pv.tp6.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ar.edu.unju.fi.pv.tp6.model.Alumno;
import ar.edu.unju.fi.pv.tp6.model.GestionAlumno;
//Clase Principal 
public class Main {
	
	public static void main(String[]args) {
		List<Alumno> listaAlumnos = new ArrayList<>(); //Crea la lista Alumnos
		GestionAlumno.coleccionAlumno(listaAlumnos);
				
		//MENU OPCIONES
		Scanner entrada = new Scanner(System.in);
		int opcion;
		do {
			System.out.println("\nMenu:");
			System.out.println("1. Agregar Alumno");
			System.out.println("2. Mostrar Lista de Alumnos");
			System.out.println("3. Buscar Alumno");
			System.out.println("4. Cantidad de Alumnos por mes y año");
			System.out.println("0. Salir");
			System.out.println("\nIngrese Opcion: ");
			opcion = entrada.nextInt();
			switch (opcion) {
			case 1://Agregar Nuevo Alumno
				GestionAlumno.agregarAlumno(entrada, listaAlumnos);
				break;
			case 2: //Mostrar Todos los Alumnos
				GestionAlumno.mostrarAlumno(entrada, listaAlumnos);
				break;
			case 3://Buscar alumno por libreta
				GestionAlumno.buscarAlumnoPorLibreta(entrada, listaAlumnos);
                break;
			case 4://Contar las cantidades de alumnos por Año y Mes
				System.out.println("Ingrese mes:");
                int mes = entrada.nextInt();
                System.out.println("Ingrese año:");
                int anio = entrada.nextInt();
                int cantidad = GestionAlumno.contarAlumnos(mes, anio, listaAlumnos);
                System.out.println("Cantidad de alumnos ingresados en " + mes + "/" + anio + ": " + cantidad + " Alumnos");
                System.out.println("\nPresione Enter para volver al Menu...");
		        entrada.nextLine(); 
		        entrada.nextLine();
				break;
			case 0:
				break;
			default://Para cualquier tecla intentar de nuevo
				System.out.println("Opcion Invalida. Intentelo de nuevo");
			}
		} while (opcion != 0);//Mientras sea distinto de cero el bucle continua
			entrada.close();
	}	
}
